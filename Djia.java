import java.util.Formatter;


public class Djia implements Comparable<Djia>
{
	private String fullEntry; // will be toString() representation
	private String date;
	private double opening; //DJIA at opening bell
	private double closing; //DJIA at closing bell
	
	public Djia(String date1, double opening1, double closing1)
	{
		date = date1;
		opening = opening1;
		closing = closing1;
		Formatter entryFormatter = new Formatter();
		entryFormatter.format("%15s%15f%15f", date, opening, closing);
		fullEntry = entryFormatter.toString();
		entryFormatter.close();
	}
	
	public String date()
	{
		return date;
	}
	
	public double opening()
	{
		return opening;
	}
	
	public double closing()
	{
		 return closing;
	}
	
	public int compareTo(Djia djiaItem)
	{
		if (this.closing < djiaItem.closing)
		{
			return -1;
		}
		
		else if (this.closing == djiaItem.closing)
		{ 
			return 0;
		}
		
		else 
		{
			return 1;
		}
	}
	public String toString()
	{
		return fullEntry;
	}
}
