# Java-Projects
A sample of my Java language projects.

DjiaApp uses a MinPQ (minimum priority queue) of Djia (Dow Jones Industrial Average) objects to show the lowest closing averages of the Dow. IndexGraph represents an adjacency list, and SCHT is a separately-chained hash table. These are just a few of the projects I completed in Data Structures I and II. 