

//import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * This priority queue will keep a queue of Comparable Objects 
 * in order from smallest to largest
 */
public class MinPQ<E extends Comparable<E>> //implements Iterable<E> 
{
	E i;
	
	private int size; //number of elements in the queue
	private Node tail = new Node(i); //the last node in the queue. 
	private Node head = new Node(i); //the head node in the queue. 
	public MinPQ() //Constructor. Empty to start with. 
	{
		head.previous = null; //should never be called
		head.data = null; // ""
		head.next = tail; 
		tail.previous = head;
		tail.data = null; //should never be called
		tail.next = null; //should never be called
		size = 0;
	}

	public int size() //Returns private data member, number of elements in queue.
	{
		return size;
	}

	public boolean isEmpty() //If empty deque, return true.
	{
		return size == 0;
	}

	/* 
	 * Takes E x as param. If x is null, throws NullPointerException.
	 * Otherwise, adds x before the first node whose data is greater
	 * than the parameter or before the tail, whichever comes first. 
	 */
	public void insert(E x)
	{
		
		Node temp = new Node(x); //create node based on x;
		boolean didRun = false; 
		if (! isEmpty()) 
		{
			//start comparisons with the node after head
			Node current = head.next;
			for (int i = 1; i <= size; i++) // comparing all nodes to temp
			{
				if (x.compareTo(current.data) <= 0) // if x <= the current 
				{
					temp.next = current; // Link temp to surrounding nodes
					temp.previous = current.previous; // ""
					current.previous.next = temp; //Link surrounding nodes to temp.
					current.previous = temp; // ""
					didRun = true; // if I get to the end of the for loop, 
					//					and this condition hasn't changed, I'll run the next if statement.
					break;
				}
				
				current = current.next;
				

			}
			
			
		}
		if (! didRun) //If temp will be the largest (or only) element in the queue
			{
				temp.next = tail;
				temp.previous = tail.previous; 
				tail.previous.next = temp;
				tail.previous = temp;
				
			}
		size ++;
	}

	/*
	 * Throws NoSuchElementException if queue is empty
	 * returns first value in queue
	 * deletes that node. 
	 */
	public E deleteMin()
	{
		E x;
		if(isEmpty())
		{
			throw new NoSuchElementException();
		}
		x = head.next.data;
		head = head.next;
		size --;
		return x;
	}
	
	public E min()
	{
		if(isEmpty())
		{
			throw new NoSuchElementException();
		}
		
		return head.next.data;
	}
	/*
	 * Returns an iterator for deque
	 * (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
//	public Iterator<E> iterator()
//	{
//		return new DequeIterator();
//	}
//
//	/*
//	 * An iterator for deque
//	 */
//	private class DequeIterator implements Iterator<E>
//	{
//		private Node current;
//		private DequeIterator()
//		{
//			current = head.next;
//		}
//		@Override
//		public boolean hasNext()
//		{
//			return current.next != null;
//		}
//
//		@Override
//		public E next()
//		{
//			if(! hasNext() )
//			{
//				throw new NoSuchElementException();
//			}
//			E x = current.data;
//			current = current.next;
//			return x;
//		}	
//
//		@Override
//		public void remove()
//		{
//			throw new UnsupportedOperationException();
//		}
//	}
	private class Node
	{
		private E data;
		private Node next;
		private Node previous;
		private Node(E x)
		{
			data = x;
			next = null;
			previous = null;
		}
	}
}

