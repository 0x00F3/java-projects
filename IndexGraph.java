// filename: IndexGraph.java
//  -- Graph represented by adjacency list of vertex indices.

import java.util.*;

public class IndexGraph
{
	private final int V; // number of vertices
	private int E;       // number of edges
	private final List<Integer>[] adj; // adjacency list

	/**
	 * Create an empty graph with V vertices.
	 */
	@SuppressWarnings("unchecked")
	public IndexGraph(int numV)
	{
		this.V = numV;
		this.E = 0;
		this.adj = new LinkedList[V];
		for (int v = 0; v < V; v++) {
			adj[v] = new LinkedList<Integer>(); // empty adj list for each vertex
		}
	}

	/**
	 * Return the number of vertices in the graph.
	 */
	public int V() { return V; }

	/**
	 * Return the number of edges in the graph.
	 */
	public int E() { return E; }

	/**
	 * Add the undirected edge v-w to graph -- both v->w and w->v, and increment E.
	 * @throws java.lang.IndexOutOfBoundsException unless both 0 <= v < V and 0 <= w < V
	 */
	public void addEdge(int v, int w)
	{
		if (0 <= v && v < this.V && 0 <= w && w < this.V)
		{
			adj[v].add(w);
			adj[w].add(v);
			E++;

		}
		else throw new java.lang.IndexOutOfBoundsException();
	}

	/**
	 * Return the list of neighbors of vertex v as in Iterable.
	 * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
	 */
	public List<Integer> adj(int v)
	{
		if (v < 0 || v >= V) throw new IndexOutOfBoundsException();
		return adj[v];
	}

	/**
	 * Return a string representation of the graph.
	 */
	public String toString()
	{
		String rep = "";
		Iterator<Integer> it;
		for(int i = 0; i < adj.length; i++)
		{
			rep += i + ": ";
			it = adj[i].iterator();
			while (it.hasNext())
			{
				rep += it.next() + " ";
				
			}
			rep += "\n";
		}
		return rep;

	}

	/**
	 * Test client.
	 */
	public static void main(String[] args)
	{
		IndexGraph g = new IndexGraph(5);
		g.addEdge(0,1);
		g.addEdge(0,3);
		g.addEdge(1,3);
		g.addEdge(2,3);
		g.addEdge(2,4);
		g.addEdge(3,4);

		System.out.println(g.toString());
	}
}

/* Output of the program

Graph with 5 vertices, 6 edges

0: 1 3
1: 0 3
2: 3 4
3: 0 1 2 4
4: 2 3

 */