// filename: SCHT.java -- CSC 301 HW#5

import java.lang.reflect.Array;

public class SCHT<E>
{
	//** instance variables
	private Node[] headArray;
	private int headLength;
	private int theSize;
	private static final int INIT_CAPACITY = 4;

	//************************************
	//  inner class Node
	//************************************
	private class Node
	{
		private E data;
		private Node next;

		private Node() { data = null; next = null; }
		private Node(E x) { data = x; next = null; }
		private Node(E x, Node after)
		{ data = x; next = after; }
	}
	//*** end class Node ****************

	public SCHT()
	{
		this(INIT_CAPACITY);
	}

	public SCHT(int initialCapacity)
	{
		headLength = initialCapacity;
		headArray = createHeadArray(headLength);
		theSize = 0;
	}

	private Node[] createHeadArray(int len)
	{
		//System.out.println("I'm going to create this headArray");
		Node[] local = (Node[]) Array.newInstance(Node.class, len);
		for (int i = 0; i < len; ++i)
			local[i] = null;
		//System.out.println("There, I created this headArray" + local.length);
		return local;
		
	}

	public int size() { return this.theSize; }

	public boolean add(E e)
	{
		//System.out.println("In SCHT, about to add");
		if (contains (e))
		{
			//System.out.println("Already here!" );
			return false;
		}
		//System.out.println("How about here?");
		boolean placed = false;
		
		int hashE = Math.abs(e.hashCode() % headArray.length);
		//System.out.println("This element should go in index" + hashE);
		//System.out.println("The length of headArray is : " + headArray.length);
		//System.out.println("But hashCode() returned: " + hashE);
		if(headArray[hashE] == null)
		{
			headArray[hashE] = new Node(e);
			placed = true;
			theSize++;
			return true;
		}
		Node current = headArray[hashE];
		while (!placed)
		{
			if (current.next == null)
			{
				current.next = new Node (e);
				placed = true;
				theSize++;
				//System.out.println("ht.size = " + size());
				return true;
			}
			else
			{
				current = current.next;
			}
		}
		theSize++;
		return true;
	}

	public void rehash(int newLength)
	{
		Node[] oldArray = headArray;
		headArray = createHeadArray(newLength);
		headLength = newLength;
		theSize = 0;
		//System.out.println("The new head length is: " + headArray.length);
		for (int i = 0; i < oldArray.length; i++)
		{
			Node current = oldArray[i];
			while (current != null)
			{
				add(current.data);
				current = current.next;
			}
		}
		
	}

	public void clear()
	{
		for (int i = 0; i < headArray.length; i++)
		{
			headArray[i] = null;
		}
		theSize = 0;
	}

	public boolean contains(Object o)
	{
		//System.out.println(o);
		if(o == null) return false;
		//System.out.println("In contains");
		int hashVal = Math.abs(o.hashCode() % headArray.length);
		//System.out.println("Called hashCode.");
		Node current = headArray[hashVal];
		while(current != null)
		{
			//System.out.println("Current.data = ");
			//System.out.println(current.data);
			if (current.data.equals(o))
			{
				//System.out.println("Contains returned true");
				return true;
			}
			else
			{
				current = current.next;
			}
		}
		//System.out.println("Contains returned false");
		return false;
	}

	public boolean isEmpty()
	{
		return (theSize == 0);
	}

	public boolean remove(Object o)
	{
		if(!contains(o)) return false;
		int hashVal = Math.abs(o.hashCode() % headArray.length);
		//System.out.println("In Remove.");
		//System.out.println("HashVal = "+ hashVal);
		//System.out.println("headArray[hashVal] = " + headArray[hashVal].data);
		//System.out.println("Object o = " + o);
		if(headArray[hashVal].data.equals(o))
		{
			headArray[hashVal] = headArray[hashVal].next;
			theSize--;
			//System.out.println("Remove() returned true");
			return true;
		}
		Node previous = headArray[hashVal];
		Node current = headArray[hashVal].next;
		while (current != null)
		{
			//System.out.println("Previous = " + previous.data);
			//System.out.println("Current = " + current.data);
			//System.out.println();
			if(current.data.equals(o))
			{
				previous.next = current.next;
				theSize--;
				//System.out.println("The size = " + size());
				return true;
			}
			previous = current;
			current = current.next;
		}
		return false;
	}

	public void print()
	{
		System.out.printf("Header table length = %d, #elements = %d\n\n", headLength, theSize);
		for (int i = 0; i < headLength; ++i) {
			System.out.printf("%2d:", i);
			for (Node ptr = headArray[i]; ptr != null; ptr = ptr.next)
				System.out.printf(" %s", ptr.data);
			System.out.print("\n");
		}
	}

	//*******************************
	// main()
	//*******************************
	public static void main(String[] args)
	{
	}
}


