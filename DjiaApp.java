import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class DjiaApp 
{

	public static void main(String[] args) 
	{
		
		//explanation
		System.out.println("This program finds the n lowest closing averages in a file ");
		System.out.println("Of Dow Jones Industrial Average closing records. ");
		System.out.println();
		
		//input from keyboard
		Scanner kbReader = new Scanner(System.in);
		System.out.print("N: ");
		int n = kbReader.nextInt();
		kbReader = new Scanner (System.in);
		System.out.print("File Name: ");
		String fileName = kbReader.nextLine();
		System.out.println();
		

		try 
		{
			kbReader = new Scanner(new File(fileName));
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("The file you have entered cannot be found. ");
			kbReader.close();
			System.exit(0);
		}
		

		//create and fill Dow Jones Priority Queue
		MinPQ<Djia> dowPQ = new MinPQ<Djia>();
		Djia current;
		String currentDate;
		double currentOpening;
		double currentClosing;
		
		while (kbReader.hasNext())
		{
			currentDate = kbReader.next();
			currentOpening = kbReader.nextDouble();
			currentClosing = kbReader.nextDouble();
			current = new Djia(currentDate, currentOpening, currentClosing);
			dowPQ.insert(current);
		}
		
		//output
		System.out.println("The " + n + " lowest closing averages are: ");
			
		System.out.println("    Date          Opening        Closing");
		for (int i = 0; i < n; i ++)
		{
			current = dowPQ.deleteMin();
			System.out.println(current.toString());
		}
		kbReader.close();
		
	}

}
